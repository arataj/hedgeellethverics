/*
 * VericsFrontend.java
 *
 * Created on Jun 23, 2015
 *
 * Copyright (c) 2015 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.languages.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BlockExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeThread;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;
import pl.gliwice.iitis.verics.parser.VericsParser;

/**
 * A frontend of a subset of Java, supported by Java2TADD.<br>
 *
 * For frontend compatibility, as the semantic checker it returns
 * <code>SemanticCheck</code> instead of <code>JavaSemanticCheck</code>,
 * and implements the functionality missing in <code>SemanticCheck</code>
 * in the method <code>semanticCheck()</code>.
 *
 * @author Artur Rataj
 */
public class VericsFrontend extends AbstractFrontend {
    /**
     * Default file name extension.
     */
    public static final String FILE_NAME_EXTENSION = "verics";
    /**
     * Creates a new instance of VericsFrontend.
     */
    public VericsFrontend() {
        super();
    }
    @Override
    public void parse(Compilation compilation, PWD topDirectory,
            String libraryPath, String fileName, CompilerException parseErrors) {
        // System.out.print("Parsing " + fileName + "...");
        // System.out.flush();
        VericsParser parser = null;
        boolean ioError = false;
        try {
                parser = new VericsParser(compilation, this, topDirectory,
                        libraryPath, fileName);
        } catch(IOException e) {
            parseErrors.addAllReports(new CompilerException(
                    null, CompilerException.Code.IO,
                    "could not compile file `" + fileName + "': " +
                    e.getMessage()));
            ioError = true;
        }
        if(!ioError)
            try {
                try {
                    parser.CompilationUnit(isLibrary(libraryPath, fileName));
                } catch(TokenMgrError f) {
                    if(f.pos != null && f.eofSeen)
                        throw new ParseException(f.pos, ParseException.Code.PARSE,
                            "unclosed comment");
                    else
                        throw f;
                }
            } catch(ParseException e) {
                e.completeStreamName(fileName);
                parseErrors.addAllReports(e);
            }
    }
    /**
     * An alternative parsing method, when there is a stream instead of a file.
     * For the Vxs compilation, when verics is used as a preprocessor.
     * 
     * @param compilation compilation to add the translation
     * results
     * @param topDirectory top directory of the compiled files
     * @param libraryPath path to the library
     * @param fileName name of the original file, if
     * an input stream is used instead can hold its name or be null 
     * @param reader reader, not null if a stream is read instead of a file
     * @param parseErrors exception to add possible parse errors
     */
    public void parseVxs(Compilation compilation,
            String libraryPath, String fileName,
            Reader reader, CompilerException parseErrors) {
        // System.out.print("Parsing " + fileName + "...");
        // System.out.flush();
        VericsParser parser = null;
        boolean ioError = false;
        parser = new VericsParser(compilation, this, reader,
                libraryPath, fileName);
        try {
            parser.CompilationUnit(isLibrary(libraryPath, fileName));
        } catch(ParseException e) {
            e.completeStreamName(fileName);
            parseErrors.addAllReports(e);
        }
    }
    @Override
    public Class getSemanticCheckClass() {
        return SemanticCheck.class;
    }
    @Override
    public void semanticCheck(Compilation compilation, SemanticCheck sc,
            BlockExpression blockExpression, CompilerException compileErrors)
            throws CompilerException {
        if(blockExpression instanceof JavaConditionalExpression) {
            JavaConditionalExpression c = (JavaConditionalExpression)
                    blockExpression;
            JavaSemanticCheck.parse(sc, c);
        } /*else if(blockExpression instanceof VericsSwitchInstanceOf) {
            VericsSwitchInstanceOf iof = (VericsSwitchInstanceOf)
                    blockExpression;
            VericsSwitchInstanceOf.parse(sc, iof);
        }*/
    }
    @Override
    public MethodSignature getMainMethodSignature(String arrayClassName,
            String stringClassName) {
        List<Type> arg = new LinkedList<>();
        MethodSignature signature = new MethodSignature(null,
                "main", arg);
        return signature;
    }
    @Override
    public void setMainMethodArgs(List<String> args, AbstractInterpreter interpreter,
            RuntimeThread thread) throws InterpreterException {
        if(!args.isEmpty())
            throw new InterpreterException(null,
                    "Verics's main class does not accept arguments");
        thread.startMethodArgs.clear();
    }
}
