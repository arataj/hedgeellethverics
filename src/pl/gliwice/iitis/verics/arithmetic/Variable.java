/*
 * Variable.java
 *
 * Created on Jun 3, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics.arithmetic;

/**
 * A probabilitic variable.
 * 
 * @author Artur Rataj
 */
public class Variable {
    /**
     * Creates a new instance of Variable.
     */
    public Variable() {
    }
}
