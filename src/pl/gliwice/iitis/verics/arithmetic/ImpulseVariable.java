/*
 * ImpulseVariable.java
 *
 * Created on Jun 4, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics.arithmetic;

/**
 * A probabilistic variable with an impulse distribution.
 * 
 * @author Artur Rataj
 */
public class ImpulseVariable extends Variable {
    /**
     * Value.
     */
    double v;
    /**
     * Probability.
     */
    double p;
    
    /**
     * Creates a new instance of ImpulseVariable. 
     * 
     * @param v                         variable
     * @param p                         probability
     */
    public ImpulseVariable(double v, double p) {
        this.v = v;
        this.p = p;
    }
}
