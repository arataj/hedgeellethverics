/*
 * BinaryOperation.java
 *
 * Created on Jun 5, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics.arithmetic;

/**
 *
 * @author Artur Rataj
 */
public abstract class BinaryOperation {
    /**
     * Result of this operation.
     */
    Variable result;
    
    /**
     * Creates a new instance of BinaryOperation.
     * 
     * @param v1                        left operand
     * @param v2                        right operand
     */
    public BinaryOperation(Variable v1, Variable v2) {
        result = getResult(v1, v2);
    }
    /**
     * Returns a result of this binary operation, if both operands
     * are impulse variables.
     * 
     * @param i1                        first impulse variable
     * @param i2                        second impulse variable
     * @return                          result of this binary operation
     *                                  on the variables
     */
    protected ImpulseVariable getResult(ImpulseVariable i1, ImpulseVariable i2) {
        ImpulseVariable out = new ImpulseVariable(i1.p*i2.p,
                operator(i1.v, i2.v));
        return out;
    }
    /**
     * Returns a result of this binary operation.
     * 
     * @param i1                        first variable
     * @param i2                        second variable
     * @return                          result of this binary operation
     *                                  on the variables
     */
    protected Variable getResult(Variable v1, Variable v2) {
        Variable out = null;
        return out;
    }
    /**
     * Defines the operator.
     * 
     * @param l                         left value
     * @param r                         right value
     * @return                          result
     */
    abstract double operator(double v, double v0);
}
