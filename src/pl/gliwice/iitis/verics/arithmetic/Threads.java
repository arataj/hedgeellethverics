/*
 * Threads.java
 *
 * Created on Jun 11, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics.arithmetic;

import java.util.*;

/**
 * A set of threads.
 * 
 * @author Artur Rataj
 */
public class Threads {
    /**
     * Variables of these threads.
     */
    HashSet<Variable> variables;
    
    /**
     * Creates a new instance of Threads. 
     */
    public Threads() {
    }
}
