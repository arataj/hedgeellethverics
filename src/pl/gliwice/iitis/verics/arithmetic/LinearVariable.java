/*
 * LinearVariable.java
 *
 * Created on Jun 4, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics.arithmetic;

/**
 * A probabilistic variable with a linear distribution.
 * 
 * @author Artur Rataj
 */
public class LinearVariable extends Variable {
    /**
     * Minimum value.
     */
    public double v1;
    /**
     * Maximum value.
     */
    public double v2;
    /**
     * Probability density at minimum value.
     */
    public double pd1;
    /**
     * Probability density at maximum value.
     */
    public double pd2;

    /**
     * Creates a new instance of LinearVariable. 
     */
    public LinearVariable(double v1, double v2, double pd1, double pd2) {
        this.v1 = v1;
        this.v2 = v2;
        this.pd1 = pd1;
        this.pd2 = pd2;
    }
}
