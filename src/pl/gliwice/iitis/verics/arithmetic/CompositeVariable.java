/*
 * CompositeVariable.java
 *
 * Created on Jun 5, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics.arithmetic;

/**
 * A set of impulse and linear threads.
 * 
 * @author Artur Rataj
 */
public class CompositeVariable {
    /**
     * Creates a new instance of CompositeVariable. 
     */
    public CompositeVariable() {
    }
}
