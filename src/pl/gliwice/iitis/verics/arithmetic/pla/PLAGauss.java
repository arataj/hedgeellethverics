/*
 * PLAExp.java
 *
 * Created on May 28, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics.arithmetic.pla;

import java.util.*;

/**
 * An approximated Gauss function y = exp((-x^2)/(2c^2)).
 * 
 * @author Artur Rataj
 */
public class PLAGauss extends PLAFunction {
    /**
     * Coefficient 2*c^2.
     */
    double _2cc;
    
    /**
     * Creates a new instance of PLAGauss.
     * 
     * @param c                         coefficient c
     */
    public PLAGauss(double c) {
        super(
                3e-4, // max error
                5e-2, // absolute diff weight
                5e-4, 5e-4, // relative diff weight and const
                1.0); // mse weight
        this._2cc = 2*c*c;
        PLAResult r = approximate(0.0, 10.0/1, 0.0, 1.1);
        List<Double> lx = r.getXList();
        List<Double> ly = r.getYList();
        List<Double> clx = new ArrayList<Double>(lx);
        List<Double> cly = new ArrayList<Double>(ly);
        Iterator<Double> i = ly.iterator();
        for(double x : lx) {
            double y = i.next();
            if(x != 0.0) {
                clx.add(0, -x);
                cly.add(0, y);
            }
        }
        approximation = new PLAResult(clx, cly);
        
    }
    @Override
    protected double getValue(double x) {
        return Math.exp(-x*x/_2cc);
    }
}
