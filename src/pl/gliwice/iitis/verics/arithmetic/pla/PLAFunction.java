/*
 * PLAFunction.java
 *
 * Created on May 28, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics.arithmetic.pla;

import java.util.*;

/**
 * A piecewise linear approximator. Extend to define the function to
 * approximate.
 * 
 * The error is mixed absolute difference/MSE.
 * 
 * @author Artur Rataj
 */
abstract public class PLAFunction {
    /**
     * Samples per segment to test.
     */
    final static int SAMPLES = 30;
    /**
     * Step multiplier when continuing going in the same direction.
     */
    final static double STEP_CONTINUE_MULT = 1.01;
    /**
     * Step multiplier when turning to the opposite direction.
     */
    final static double STEP_TURNING_MULT = 0.1;
    
    /**
     * Maximum allowed error.
     */
    double maxAllowedErr;
    /**
     * Weight of maximum absolute difference in error, that is,
     * in the weighted sum of maximum absolute difference,
     * maximum relative difference and MSE.
     */
    double madWeight;
    /**
     * Weight of maximum relative difference in error, that is,
     * in the weighted sum of maximum absolute difference,
     * maximum relative difference and MSE.
     */
    double mrdWeight;
    /**
     * MRD constant added to the mean absolute value.
     */
    double mrdConst;
    /**
     * Weight of MSE in error, that is, in the weighted
     * sum of maximum absolute difference, maximum relative difference
     * and MSE.
     */
    double mseWeight;
    /**
     * Resulting approximation or null if none.
     */
    PLAResult approximation;
    
    /**
     * Creates a new instance of PLAFunction. 
     * 
     * @param maxAllowedErr             maximum error
     * @param madRatio                  weight of maximum absoulte
     *                                  difference in error
     * @param mrdRatio                  weight of maximum relative
     *                                  difference in error
     * @param mrdConst                  MRD constant added to the
     *                                  mean absolute value
     * @param mseRatio                  weight of MSE in error
     */
    public PLAFunction(double maxAllowedErr, double madWeight,
            double mrdWeight, double mrdConst, double mseWeight) {
        this.maxAllowedErr = maxAllowedErr;
        this.madWeight = madWeight;
        this.mrdWeight = mrdWeight;
        this.mrdConst = mrdConst;
        this.mseWeight = mseWeight;
        approximation = null;
    }
    /**
     * Returns value of the approximated function.
     * 
     * @param x                         argument
     * @return                          value
     */
    abstract protected double getValue(double x);
    /**
     * Finds the error for a segment.
     * 
     * @param x0 left x coordinate
     * @param y0 left y coordinate
     * @param x1 right x coordinate
     * @param y1 right y coordinate
     * @return maximum error
     */
    double findError(double x0, double y0, double x1, double y1) {
        double xDiff = x1 - x0;
        double yDiff = y1 - y0;
        double maxDiff = 0.0;
        double meanAbsValue = 0.0;
        double mse = 0.0;
        for(int s = 0; s < SAMPLES; ++s) {
            double f = (double)s/(SAMPLES - 1);
            double x = x0 + xDiff*f;
            double y = y0 + yDiff*f;
            double v = getValue(x);
            double diff = Math.abs(v - y);
            if(maxDiff < diff)
                maxDiff = diff;
            meanAbsValue += Math.abs(v);
            mse += Math.pow(getValue(x) - y, 2.0);
        }
        meanAbsValue = meanAbsValue/SAMPLES;
        double maxRelativeDiff = maxDiff/(
                mrdConst + meanAbsValue);
        mse = mse/SAMPLES;
        return maxDiff*madWeight + maxRelativeDiff*mrdWeight +
                mse*mseWeight;
    }
    /**
     * Finds the error for a set of segments.
     * 
     * @param xt                        x coordinates of subsequent
     *                                  nodes to relax
     * @param yt                        y coordinates of subsequent
     *                                  nodes to relax
     * @return maximum error
     */
    double findError(double[] xt, double[] yt) {
        double maxErr = 0;
        for(int node = 0; node < xt.length - 1; ++node) {
            double currMaxErr = findError(
                    xt[node + 1], yt[node + 1], xt[node], yt[node]);
            System.out.println(node + ":" + currMaxErr);
            if(maxErr < currMaxErr)
                maxErr = currMaxErr;
        }
        return maxErr;
    }
    /**
     * Finds initial node x coordinates.
     * 
     * @param minX                      minimum argument
     * @param maxX                      maximum argument
     * @return                          x coordinates of subsequent nodes
     */
    double[] findX(double min, double max) {
        final double STEP_FORWARD_MULT = 1.01;
        final double STEP_TURN_BACK_MULT = 0.1;
        List<Double> nodes = new LinkedList<Double>();
        final double MIN_STEP = 1e-6;
        double step = MIN_STEP;
        nodes.add(min);
        double beg = min;
        do {
            double curr = beg;
            boolean forward = true;
            boolean firstForward = true;
            double lastFitting = curr;
            while(curr < max) {
                if(forward)
                    curr += step;
                else
                    curr -= step;
                //System.out.println("curr = " + curr);
                boolean fits = findError(beg, getValue(beg), curr, getValue(curr)) <
                        maxAllowedErr;
                if(fits) {
                    lastFitting = curr;
                    step *= STEP_FORWARD_MULT;
                    forward = true;
                    //System.out.println("fits");
                } else {
                    if(step > MIN_STEP) {
                        if(forward) {
                            step *= STEP_TURN_BACK_MULT;
                            System.out.print("<");
                        }
                        forward = false;
                        firstForward = false;
                        //System.out.println("not fits, forward = " + forward);
                    } else
                        break;
                }
                //System.out.println("step = " + step);
            }
            System.out.println("last fitting = " + lastFitting);
            beg = lastFitting;
            if(beg > max)
                beg = max;
            nodes.add(beg);
        } while(beg < max);
        double[] array = new double[nodes.size()];
        int i = 0;
        for(double d : nodes)
            array[i++] = d;
        return array;
    }
    /**
     * Finds the error that is minimal in a set of segments.
     * 
     * @param xt                        x coordinates of subsequent
     *                                  nodes to relax
     * @param yt                        y coordinates of subsequent
     *                                  nodes to relax
     * @return minimum maximum error
     */
    double findMinError(double[] xt, double[] yt) {
        double minErr = Double.MAX_VALUE;
        for(int node = 0; node < xt.length - 1; ++node) {
            double currMaxErr = findError(
                    xt[node + 1], yt[node + 1], xt[node], yt[node]);
            System.out.println(node + ":" + currMaxErr);
            if(minErr > currMaxErr)
                minErr = currMaxErr;
        }
        return minErr;
    }
    /**
     * Finds maximum error at neighboring segments of
     * a node, assuming the node has a given position.
     * 
     * @param xt                        x coordinates of subsequent nodes
     * @param yt                        y coordinates of subsequent nodes
     * @param node                      index of the tested node
     * @param x                         tested x position of the node
     * @param y                         tested y position of the node
     */
    double findErrorAtNode(double[] xt, double[] yt, int node,
            double x, double y) {
        return Math.max(
                    node == 0 ? 0 :
                        findError(xt[node - 1], yt[node - 1], x, y),
                    node == xt.length - 1 ? 0 :
                        findError(xt[node + 1], yt[node + 1], x, y)
                    );
    }
    /**
     * Relaxes node x coordinates.
     * 
     * @param xt                        x coordinates of subsequent
     *                                  nodes to relax
     * @param yt                        y coordinates of subsequent
     *                                  nodes to relax
     * @return                          maximum offset of x coordinate
     */
    double relaxX(double[] xt, double[] yt) {
        double maxOffset = 0.0;
        final double MIN_STEP = 1e-6;
        for(int node = 1; node < xt.length - 1; ++node) {
            double step = MIN_STEP;
            double beg = xt[node];
            double maxErrC = findErrorAtNode(xt, yt, node,
                    beg, yt[node]);
            double begL = beg - step;
            double maxErrL = findErrorAtNode(xt, yt, node,
                    begL, getValue(begL));
            double begR = beg + step;
            double maxErrR = findErrorAtNode(xt, yt, node,
                    begR, getValue(begR));
            if(maxErrC <= maxErrL && maxErrC <= maxErrR)
                // already relaxed
                continue;
            else {
                double stepMult = maxErrR < maxErrL ? 1.0 : -1.0;
                double bestMaxErr = maxErrC;
                double curr = beg;
                boolean forward = true;
                boolean firstForward = true;
                double lastFitting = curr;
                while(step >= MIN_STEP && curr > xt[node - 1] && curr < xt[node + 1]) {
                    if(forward)
                        curr += step*stepMult;
                    else
                        curr -= step*stepMult;
                    //System.out.println("curr = " + curr);
                    double currMaxErr = findErrorAtNode(xt, yt, node,
                        curr, getValue(curr));
                    if(bestMaxErr > currMaxErr) {
                        bestMaxErr = currMaxErr;
                        lastFitting = curr;
                        if(!forward)
                            step *= STEP_TURNING_MULT;
                        else
                            step *= STEP_CONTINUE_MULT;
                        forward = true;
                        //System.out.println("better");
                    } else {
                        if(forward) {
                            step *= STEP_TURNING_MULT;
                            System.out.print("<");
                        } else if(curr*stepMult < lastFitting*stepMult)
                            break;
                        forward = false;
                        firstForward = false;
                    }
                    //System.out.println("step = " + step);
                }
                double prevX = xt[node];
                xt[node] = Math.min(Math.max(xt[node - 1], lastFitting), xt[node + 1]);
                double offset = Math.abs(xt[node] - prevX);
                if(maxOffset < offset)
                    maxOffset = offset;
                yt[node] = getValue(xt[node]);
            }
        }
        System.out.println("**" + maxOffset);
        return maxOffset;
    }
    /**
     * Relaxes node y coordinates.
     * 
     * @param xt                        x coordinates of subsequent
     *                                  nodes to relax
     * @param yt                        y coordinates of subsequent
     *                                  nodes to relax
     * @param minY                      minimum value
     * @param maxY                      maximum value
     * @return                          maximum offset of y coordinate
     */
    double relaxY(double[] xt, double[] yt, double minY, double maxY) {
        double maxOffset = 0.0;
        final double MIN_STEP = 1e-6;
        for(int node = 0; node < xt.length; ++node) {
            double step = MIN_STEP;
            double beg = yt[node];
            double v = xt[node];
            double maxErrC = findErrorAtNode(xt, yt, node,
                    v, beg);
            double begL = beg - step;
            double maxErrL = findErrorAtNode(xt, yt, node,
                    v, begL);
            double begR = beg + step;
            double maxErrR = findErrorAtNode(xt, yt, node,
                    v, begR);
            if(maxErrC <= maxErrL && maxErrC <= maxErrR)
                // already relaxed
                continue;
            else {
                double stepMult = maxErrR < maxErrL ? 1.0 : -1.0;
                double bestMaxErr = maxErrC;
                double curr = beg;
                boolean forward = true;
                boolean firstForward = true;
                double lastFitting = curr;
                while(step >= MIN_STEP) {
                    if(forward) {
                        curr += step*stepMult;
                    } else
                        curr -= step*stepMult;
                    if(curr < minY || curr > maxY)
                        break;
                    //System.out.println("curr = " + curr);
                    double currMaxErr = findErrorAtNode(xt, yt, node,
                        v, curr);
                    if(bestMaxErr > currMaxErr) {
                        bestMaxErr = currMaxErr;
                        lastFitting = curr;
                        if(!forward)
                            step *= STEP_TURNING_MULT;
                        else
                            step *= STEP_CONTINUE_MULT;
                        forward = true;
                        //System.out.println("better");
                    } else {
                        if(forward) {
                            step *= STEP_TURNING_MULT;
                            System.out.print("<");
                        } else if(curr*stepMult < lastFitting*stepMult)
                            break;
                        forward = false;
                        firstForward = false;
                    }
                    //System.out.println("step = " + step);
                }
                double offset = Math.abs(yt[node] - lastFitting);
                if(maxOffset < offset)
                    maxOffset = offset;
                yt[node] = lastFitting;
            }
        }
        System.out.println("**Y" + maxOffset);
        return maxOffset;
    }
    /**
     * Updates y coordinates to values of approximated function
     * at given x values.
     * 
     * @param xt                        x coordinates of subsequent
     *                                  nodes to relax
     * @param yt                        y coordinates of subsequent
     *                                  nodes to relax
     */
    void updateY(double[] xt, double[] yt) {
        for(int node = 0; node < xt.length; ++node)
            yt[node] = getValue(xt[node]);
    }
    /**
     * Produces the approximation.
     * 
     * @param minX                      minimum argument
     * @param maxX                      maximum argument
     * @param minY                      minimum value
     * @param maxY                      maximum value
     */
    @SuppressWarnings("empty-statement")
    protected PLAResult approximate(double minX, double maxX,
            double minY, double maxY) {
        double[] xt = findX(minX, maxX);
        int num = xt.length;
        double[] yt = new double[num];
        updateY(xt, yt);
        for(int iteration = 0; iteration < 2; ++iteration) {
            while(relaxX(xt, yt) > (maxX - minX)/1000)
                ;
            while(relaxY(xt, yt, minY, maxY) > maxAllowedErr/1000)
                ;
        }
        return new PLAResult(xt, yt);
    }
    /**
     * Returns the computed approximation.
     * 
     * @return                          list of subsequent approximating
     *                                  nodes
     */
    public PLAResult getApproximation() {
        return approximation;
    }
}
