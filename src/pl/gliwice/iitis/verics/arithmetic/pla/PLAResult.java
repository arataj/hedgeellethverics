/*
 * PLAResult.java
 *
 * Created on May 28, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics.arithmetic.pla;

import java.util.*;

/**
 * List of nodes being the result of approximation.
 * 
 * @author Artur Rataj
 */
public class PLAResult {
    /**
     * X codrinates.
     */
    public double[] xt;
    /**
     * Y coordinates.
     */
    public double[] yt;
    /**
     * Number of points.
     */
    public int num;
    
    /**
     * Creates a new instance of PLAResult, from arrays.
     * 
     * @param xt                        x coordinates of subsequent
     *                                  nodes to relax
     * @param yt                        y coordinates of subsequent
     *                                  nodes to relax
     */
    public PLAResult(double[] xt, double[] yt) {
        this.xt = xt;
        this.yt = yt;
        num = xt.length;
    }
    /**
     * Creates a new instance of PLAResult, from lists.
     * 
     * @param xl                        x coordinates of subsequent
     *                                  nodes to relax
     * @param yl                        y coordinates of subsequent
     *                                  nodes to relax
     */
    public PLAResult(List<Double> xl, List<Double> yl) {
        num = xl.size();
        xt = new double[num];
        yt = new double[num];
        Iterator<Double> xli = xl.iterator();
        Iterator<Double> yli = yl.iterator();
        for(int i = 0; i < num; ++i) {
            xt[i] = xli.next();
            yt[i] = yli.next();
        }
    }
    /**
     * Returns x coordinates as a list.
     * 
     * @return                          list of x coordinates of
     *                                  subsequent nodes
     */
    public List<Double> getXList() {
        List<Double> xl = new ArrayList<Double>();
        for(int i = 0; i < num; ++i)
            xl.add(xt[i]);
        return xl;
    }
    /**
     * Returns y coordinates as a list.
     * 
     * @return                          list of y coordinates of
     *                                  subsequent nodes
     */
    public List<Double> getYList() {
        List<Double> yl = new ArrayList<Double>();
        for(int i = 0; i < num; ++i)
            yl.add(yt[i]);
        return yl;
    }
    @Override
    public String toString() {
        String s = "";
        for(int i = 0; i < num; ++i)
            s += xt[i] + "\t" + yt[i] + "\n";
        return s;
    }
}
