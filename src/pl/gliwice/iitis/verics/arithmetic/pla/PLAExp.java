/*
 * PLAExp.java
 *
 * Created on May 28, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.verics.arithmetic.pla;

import java.util.*;

/**
 * An approximated exponent y = exp(ax).
 * 
 * @author Artur Rataj
 */
public class PLAExp extends PLAFunction {
    /**
     * Coefficient a.
     */
    double a;
    
    /**
     * Creates a new instance of PLAExp.
     * 
     * @param a                         coefficient a
     */
    public PLAExp(double a) {
        super(
                10e-5, // max error
                5e-5, // absolute diff weight
                5e-4, 5e-4, // relative diff weight and const
                1.0); // mse weight
        this.a = a;
        approximation = approximate(0, -40.0/a, 0.0, 1.0);
    }
    @Override
    protected double getValue(double x) {
        return Math.exp(a*x);
    }
}
